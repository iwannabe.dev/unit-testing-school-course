class StringCalculator:
    def add(self, numbers_as_string):
        if not numbers_as_string:
            return 0
        else:
            numbers = numbers_as_string.split(",")
            result = 0

            for number in numbers:
                result += int(number)

            return result

# number_as_string = "3,7"
# calculator = StringCalculator()
# print(calculator.add(number_as_string))