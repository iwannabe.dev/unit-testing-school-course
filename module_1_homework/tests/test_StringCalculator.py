import unittest
from module_1_homework.string_calculator import StringCalculator

class TestStringCalculator(unittest.TestCase):
    def setUp(self) -> None:
        self.calculator = StringCalculator()

    def test_outputType(self):
        self.assertIsInstance(self.calculator.add("4"), int)
        self.assertIsInstance(self.calculator.add("3,4"), int)
        self.assertIsInstance(self.calculator.add("-5,-3"), int)

    def test_methodReturnsSomething(self):
        self.assertIsNotNone(self.calculator.add("4"))
        self.assertIsNotNone(self.calculator.add("3,4,8"))
        self.assertIsNotNone(self.calculator.add("-5,-3"))

    def test_add(self):
        self.assertEqual(self.calculator.add("4"), 4)
        self.assertEqual(self.calculator.add("3,4"), 7)
        self.assertEqual(self.calculator.add("-5,-3"), -8)
        self.assertEqual(self.calculator.add("5,3,2"), 10)
        self.assertEqual(self.calculator.add("1,2,3,4,5"), 15)


if __name__ == '__main__':
    unittest.main()