from module_2_lesson_3.product import Product


class VatService:
    def __init__(self, vat_value=0.23):
        self.vat_value = vat_value

    def get_gross_price_for_default_vat(self, product):
        return self.get_gross_price(product, self.vat_value)

    def get_gross_price(self, product, vat_value):
        if vat_value >= 1:
            raise Exception("VAT must be lower!")
        return product.net_price + (product.net_price * vat_value)