import unittest
from module_2_lesson_3.vat_service import VatService
from module_2_lesson_3.product import Product


class TestVatService(unittest.TestCase):
    vat_service = VatService()

    def setUp(self) -> None:
        vat_service = VatService()

    def generate_product(self, net_price):
        id = 1  # should be generated, but lets leave it for simplicity of this exercise
        return Product(id, net_price)

    def test_get_gross_price_for_default_vat(self):
        # given
        product = self.generate_product(20.00)

        # when
        result = self.vat_service.get_gross_price_for_default_vat(product)

        # then
        self.assertEqual(24.60, result)


    def test_get_gross_price(self):
        # given
        product = self.generate_product(10.00)
        vat = 0.08

        # when
        result = self.vat_service.get_gross_price(product, vat)

        # then
        self.assertEqual(10.80, result)

    def test_raise_exception_when_vat_is_too_high(self):
        # given
        product = self.generate_product(10.00)
        vat = 2.00

        # then
        self.assertRaises(Exception, lambda : self.vat_service.get_gross_price(product, vat))


if __name__ == "__main__":
    unittest.main(argv=[''], verbosity=2, exit=False)